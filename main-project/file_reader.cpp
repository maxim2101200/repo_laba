#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.second = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.minute = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.hour = atoi(str_number);
    return result;
}

void read(const char* file_name, RACE_RESULT* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            RACE_RESULT* item = new RACE_RESULT;
            file >> item->runner.last_name;
            file >> item->runner.first_name;
            file >> item->runner.middle_name;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file >> item->author.last_name;
            file >> item->author.first_name;
            file >> item->author.middle_name;
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(item->title, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}