#ifndef FILTER_H
#define FILTER_H

#include "RACE_RESULT.h"

RACE_RESULT** filter(RACE_RESULT* array[], int size, bool (*check)(RACE_RESULT* element), int& result_size);

bool check_book_subscription_by_author(RACE_RESULT* element);

bool check_RACE_RESULT_by_time(RACE_RESULT* element);

#endif
