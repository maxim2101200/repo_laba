#include <iostream>
#include <iomanip>

using namespace std;

#include "RACE_RESULT.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Лабораторная работа №9. GIT\n";
    cout << "Вариант №1. Результаты марафона\n";
    cout << "Автор: Якушевич Максим\n\n";
    RACE_RESULT* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Библиотечный абонемент *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** вывод читателя **********/
            cout << "Участник........: ";
            // вывод фамилии
            cout << subscriptions[i]->runner.last_name << " ";
            // вывод первой буквы имени
            cout << subscriptions[i]->runner.first_name[0] << ". ";
            // вывод первой буквы отчества
            cout << subscriptions[i]->runner.middle_name[0] << ".";
            cout << '\n';
            // вывод названия
            cout << '"' << subscriptions[i]->title << '"';
            cout << '\n';
            /********** вывод даты выдачи **********/
            // вывод года
            cout << "Время старта.....: ";
            cout << setw(4) << setfill('0') << subscriptions[i]->start.hour << '-';
            // вывод месяца
            cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << '-';
            // вывод числа
            cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
            cout << '\n';
            /********** вывод даты возврата **********/
            // вывод года
            cout << "Время финиша...: ";
            cout << setw(4) << setfill('0') << subscriptions[i]->finish.hour << '-';
            // вывод месяца
            cout << setw(2) << setfill('0') << subscriptions[i]->finish.minute << '-';
            // вывод числа
            cout << setw(2) << setfill('0') << subscriptions[i]->finish.second;
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
}
