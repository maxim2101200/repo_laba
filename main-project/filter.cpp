#include "filter.h"
#include <cstring>
#include <iostream>

RACE_RESULT** filter(RACE_RESULT* array[], int size, bool (*check)(RACE_RESULT* element), int& result_size)
{
	RACE_RESULT** result = new RACE_RESULT * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_RACE_RESULT_by_time(RACE_RESULT* element)
{
	return element->start.minute == 00 && element->start.hour == 14;
}

