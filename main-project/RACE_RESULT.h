#ifndef RACE_RESULT_H
#define RACE_RESULT_H

#include "constants.h"

struct date
{
    int second;
    int minute;
    int hour;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct RACE_RESULT
{
    person runner;
    date start;
    date finish;
    person author;
    char title[MAX_STRING_SIZE];
};

#endif