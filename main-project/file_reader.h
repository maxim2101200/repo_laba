
#ifndef FILE_READER_H
#define FILE_READER_H

#include "RACE_RESULT.h"

void read(const char* file_name, RACE_RESULT* array[], int& size);

#endif

#pragma once

